# Projet prise de rendez-vous

Utilisation de FullCalendar : https://fullcalendar.io/

Etapes pour lancer l'application :
```
dans un terminal se mettre dans le répertoire du projet
$ npm init (bien choisir entry point : server.js)
```

Puis lancer  :
```
$ npm install 
```

Pour la base de données :
```
$ sqlite3 db.sqlite
sqlite> CREATE TABLE user (email TEXT PRIMARY KEY, firstname TEXT, lastname TEXT ,password TEXT)
Terminez le programme en utilisant le caractère de fin de fichier (CTRL + D)
```
