"use strict"

const Sqlite = require('better-sqlite3');
const db = new Sqlite('db.sqlite');

const load = function (){
    db.prepare('CREATE TABLE IF NOT EXISTS user (email TEXT PRIMARY KEY, firstname TEXT, lastname TEXT ,password TEXT)').run();
}

load();