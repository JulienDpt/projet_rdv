"use strict"

const Sqlite = require('better-sqlite3');
const bcrypt = require('bcrypt');

function crypt_password(password) {
    return bcrypt.hashSync(password,10);
}

function compare_password(password, saved_hash) {
    return bcrypt.compareSync(password, saved_hash) == true;
}

const db = new Sqlite('db.sqlite');

function add_user(email, firstname, lastname, password) {
    if(!email ||  !firstname|| !lastname || !password) return -1;
    try {
        let insert = db.prepare(`INSERT INTO user VALUES (?, ?, ?, ?)`);
        let result = insert.run([email, firstname, lastname,crypt_password(password)]);
        return result.lastInsertRowid;
    } catch(e) {
        if(e.code == 'SQLITE_CONSTRAINT_PRIMARYKEY') return -1;
        throw e;
    }
}

function get_user_name(id) {
    let select = db.prepare('SELECT firstname FROM user WHERE rowid = ?');
    let result = select.get([id]);
    if(result) return result.firstname;
    return null;
}

function login(email, password) {
    let select = db.prepare(`SELECT rowid, password FROM user WHERE email = ?`);
    let result = select.get(email);
    if(result && compare_password(password, result.password)) return result.rowid;
    return -1;
}

module.exports = {
    add_user: add_user,
    get_user_name: get_user_name,
    login: login,
};
