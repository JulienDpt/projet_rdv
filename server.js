"use strict"

const express = require('express');
const app = express();

const model = require('./model/model.js');

const mustache = require('mustache-express');

const cookieSession = require('cookie-session');
app.use(cookieSession({
    secret: 'mot-de-passe-du-cookie',
}));

app.use(express.static('assets'));

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

app.engine('html', mustache());
app.set('view engine', 'html');
app.set('views', './views');

function add_info(req, res, next) {
    const info = {
        'not-authenticated': {type: 'danger', text: "Erreur : cette page requiert de se connecter"},
        'invalid-credentials': {type: 'danger', text: "Erreur : login ou mot de passe invalide"},
        'invalid-user': {type: 'danger', text: "Erreur : l'utilisateur existe déjà ou est invalide"},
        'password-mismatch': {type: 'danger', text: "Erreur : les mots de passe ne concordent pas"},
        'logged-in': {type: 'info', text: "Info : utilisateur connecté"},
        'logged-out': {type: 'danger', text: "Info : utilisateur déconnecté"},
    }
    if(req.query.info && req.query.info in info) {
        res.locals.info = info[req.query.info];
    }
    return next();
}
app.use(add_info);


function updateLocals(req,res,next){
    if(req.session.user){             //Teste si l'utilisateur est connecté
        res.locals.authenticated = true
    }
    next();
}

// middleware pour verifier si l'utilisateur est connecté
function is_authenticated(req, res, next) {
    if(req.session.user) {
        return next();
    }
    res.redirect("/?info=not-authenticated")
}

app.use(updateLocals) // Utilise notre middleware updateLocals sur toutes nos routes


app.post('/login', (req, res) => {
    if(!('email' in req.body && 'password' in req.body)) {
        res.status(400).send('invalid request');
        return;
    }
    const user = model.login(req.body.email, req.body.password);
    console.log(user);
    if(user != -1) {
        req.session.user = user;
        res.redirect('/index?info=logged-in');
    } else {
        res.redirect('/?info=invalid-credentials');
    }
});

app.post('/logout', is_authenticated, (req, res) => {
    req.session = null;
    res.redirect('/?info=logged-out')
});

app.post('/new_user', (req, res) => {
    if(!('firstname' in req.body && 'lastname' in req.body && 'email' in req.body && 'password' in req.body && 'confirm_password' in req.body)) {
        res.status(400).send('invalid request');
        return;
    }
    if(req.body.password != req.body.confirm_password) {
        res.redirect('/?info=password-mismatch');
    } else {
        const user = model.add_user(req.body.email, req.body.firstname, req.body.lastname ,req.body.password);
        if(user != -1) {
            req.session.user = user;
            res.redirect('/index?info=logged-in');
        } else {
            res.redirect('/?info=invalid-user');
        }
    }
});

app.get('/', (req, res) => {
    res.render('login');
});

app.get('/index', is_authenticated, (req, res) => {
    const data = {
        name : model.get_user_name(req.session.user)
    }
    res.render('main', data);
});


app.listen(3000, () => console.log('listening on http://localhost:3000'));
