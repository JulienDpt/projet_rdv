const date = new Date();

document.addEventListener('DOMContentLoaded', function() { //TODO affichage différent en fonction de l'agent choisi avec les créneaux dispo
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        eventDurationEditable: false,
        eventStartEditable : false,
        allDaySlot : false,
        expandRows: true,
        slotMinTime: '08:00',
        slotMaxTime: '18:00',
        businessHours: {
            daysOfWeek: [ 1, 2, 3, 4, 5 ],
            startTime: '08:00',
            endTime: '18:00',
        },
        locale : 'fr',
        initialDate: date.toISOString(),
        initialView: 'timeGridWeek',
        headerToolbar: {
            left: 'prev,next,addEventButton',
            center: 'title',
            right: 'timeGridWeek,timeGridDay'
        },
        editable: true,
        selectable: true,
        selectMirror: true,
        events: [ // TODO parcourir la BD pour afficher les events
            {
                title: 'My Event',
                start: "2021-06-15T12:00:00",
                end: "2021-06-15T12:30:00",
            }
            // other events here
        ],
        select : function (info) {
            calendar.addEvent({
                start : info.startStr,
                end : info.endStr,
                title: info.startStr + " " + info.endStr
            })
            // TODO insertion dans une base de données events
            // TODO affichage d'une fenêtres
        },
    });

    calendar.render();
});
